
        import path from 'path'

        const DOCS_PATH = path.join(__dirname, 'docs');

        export default {
          entry: path.join(__dirname, 'src', 'index.tsx'),

          getRoutes: async () => {
            return [{ path: '/', template: '_DocPage' }, { path: '404', template: '_DocPage' }, ]
          },

          basePath: 'test-docs',

          plugins: [
            'react-static-plugin-typescript',
            'react-static-plugin-styled-components',
            [
              '@riboseinc/react-static-plugin-aperis-doc-pages',
              {
                sourcePath: DOCS_PATH,
                urlPrefix: '',
                title: "Documentation site",
                headerBanner: 'header-banner.svg',
                footerBanner: 'footer-banner.svg',
                footerBannerLink: "https://open.ribose.com/",
              },
            ],
            require.resolve('react-static-plugin-reach-router'),
            require.resolve('react-static-plugin-sitemap'),
            [
              'react-static-plugin-file-watch-reload',
              {
                paths: [`${DOCS_PATH}/**/*`],
              },
            ],
          ],
        }
      